package com.kpc.criteria.repo;

import com.kpc.criteria.dto.request.FilterRequest;
import com.kpc.criteria.dto.request.Filtered;
import com.kpc.criteria.dto.request.Sorted;
import com.kpc.criteria.entity.Post;
import com.kpc.criteria.entity.Post_;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PostCriteriaRepositoryImpl implements PostCriteriaRepository {

  @Autowired
  private EntityManager em;

  List<String> filterableFields = List.of(Post_.ID, Post_.TITLE, Post_.BODY, Post_.USER_ID);

  List<String> sortableFields = List.of(Post_.ID);

  public List<Post> searchPost(FilterRequest filterRequest) {
    filterRequest = Objects.requireNonNullElse(filterRequest, new FilterRequest());

    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Post> cr = cb.createQuery(Post.class);
    Root<Post> root = cr.from(Post.class);
    List<Predicate> predicates = new LinkedList<>();

    getFilterableFields(filterableFields, filterRequest.getFiltered()).forEach(filtered -> {
      predicates.add(cb.like(root.get(filtered.getKey()), "%" + filtered.getValue() + "%"));
    });

    List<Order> orders = new LinkedList<>();
    getSortableFields(sortableFields, filterRequest.getSorted()).forEach(sorted -> {
      if (sorted.isDesc()) {
        orders.add(cb.desc(root.get(sorted.getKey())));
      } else {
        orders.add(cb.asc(root.get(sorted.getKey())));
      }
    });

    int pageNumber = Objects.requireNonNullElse(filterRequest.getPage(), 0);
    int pageSize = Objects.requireNonNullElse(filterRequest.getPageSize(), 20);

    cr.select(root).where(predicates.toArray(new Predicate[0])).orderBy(orders);
    TypedQuery<Post> query = em.createQuery(cr);
    query.setFirstResult(pageNumber * pageSize);
    query.setMaxResults(pageSize);
    return query.getResultList();
  }

  private List<Filtered> getFilterableFields(List<String> filterableFields, List<Filtered> filter) {
    if (Objects.isNull(filter) || filter.isEmpty()) {
      return Collections.emptyList();
    }
    return filter.stream().filter(filtered -> filterableFields.contains(filtered.getKey())).collect(Collectors.toList());
  }

  private List<Sorted> getSortableFields(List<String> sortableFields, List<Sorted> sorter) {
    if (Objects.isNull(sorter) || sorter.isEmpty()) {
      return Collections.emptyList();
    }
    return sorter.stream().filter(filtered -> sortableFields.contains(filtered.getKey())).collect(Collectors.toList());
  }
}
