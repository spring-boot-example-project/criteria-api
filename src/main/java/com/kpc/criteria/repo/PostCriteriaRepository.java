package com.kpc.criteria.repo;

import com.kpc.criteria.dto.request.FilterRequest;
import com.kpc.criteria.entity.Post;
import java.util.List;

public interface PostCriteriaRepository {

  List<Post> searchPost(FilterRequest filterRequest);
}
