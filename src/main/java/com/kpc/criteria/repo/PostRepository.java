package com.kpc.criteria.repo;

import com.kpc.criteria.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>, PostCriteriaRepository {}
