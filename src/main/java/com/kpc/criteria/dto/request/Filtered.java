package com.kpc.criteria.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Filtered {

  private String key;

  private String value;

}
