package com.kpc.criteria.dto.request;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterRequest {

  private Integer page;

  private Integer pageSize;

  private List<Filtered> filtered;

  private List<Sorted> sorted;
}
