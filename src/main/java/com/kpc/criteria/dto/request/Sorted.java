package com.kpc.criteria.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Sorted {

  private String key;

  private boolean desc;

}
