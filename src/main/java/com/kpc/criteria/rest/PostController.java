package com.kpc.criteria.rest;

import com.kpc.criteria.dto.request.FilterRequest;
import com.kpc.criteria.entity.Post;
import com.kpc.criteria.repo.PostRepository;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/posts")
public class PostController {

  @Autowired
  private PostRepository postRepository;

  @GetMapping
  public List<Post> getPosts(@RequestBody(required = false) FilterRequest filter){
    return postRepository.searchPost(filter);
  }
}
