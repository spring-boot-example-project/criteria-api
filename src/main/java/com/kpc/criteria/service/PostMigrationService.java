package com.kpc.criteria.service;

import com.kpc.criteria.entity.Post;
import com.kpc.criteria.repo.PostRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class PostMigrationService {

  public static String POST_URL = "https://jsonplaceholder.typicode.com";

  private final WebClient webClient;

  private final PostRepository postRepository;

  public PostMigrationService(WebClient.Builder webClientBuilder, PostRepository postRepository) {
    this.webClient = webClientBuilder.baseUrl(POST_URL).build();
    this.postRepository = postRepository;
  }

  public void savePosts(){
    List<Post> x = fetchPosts();
    postRepository.saveAll(x);
  }

  private List<Post> fetchPosts(){
    return this.webClient.get().uri("/posts")
                         .retrieve().bodyToFlux(Post.class).collect(Collectors.toList()).share().block();
  }


}
