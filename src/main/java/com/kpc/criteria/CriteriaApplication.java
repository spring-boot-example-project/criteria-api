package com.kpc.criteria;

import com.kpc.criteria.service.PostMigrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CriteriaApplication implements CommandLineRunner {

  public static void main(String[] args) {
    SpringApplication.run(CriteriaApplication.class, args);
  }

  @Autowired
  private PostMigrationService postMigrationService;

  @Override
  public void run(String... args) throws Exception {
    postMigrationService.savePosts();
  }
}
