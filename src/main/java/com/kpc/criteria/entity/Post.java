package com.kpc.criteria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Post {

  @Id
  private Long id;

  private Long userId;

  @Column(length = 1000)
  private String title;

  @Column(length = 1000)
  private String body;
}
