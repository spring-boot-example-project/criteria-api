## Aim
Creating a generic repository with the criteria api

## Detail
* The project uses H2DB(inmemory) database
* the post data retrieved by making a get request to jsonplaceholder.typicode.com api
* The received data is written to the in memory database
* Requests are met via API  **/posts** api 

## Sample request
```curl
curl --location --request GET 'http://localhost:8080/posts' \
--header 'Content-Type: application/json' \
--data-raw '{
"filtered": [
{
"key" : "title",
"value": "sa"
},
{
"key" : "body",
"value": "men"
}
],
"sorted": [
{
"key": "id",
"desc": false
}
],
"page":0,
"pageSize":10
}'
```